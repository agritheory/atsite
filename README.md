# atsite

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Build & push container
```
docker build -t registry.gitlab.com/agritheory/atsite .
docker push registry.gitlab.com/agritheory/atsite
```

### Run to check if container was built correctly
```
docker run -p 80:80 registry.gitlab.com/agritheory/atsite
```
Then open localhost in browser.

### Create deployment
```
kubectl run NAME --port=80 --image=registry.gitlab.com/agritheory/atsite:latest
```

### Expose deployment
```
kubectl expose deployment NAME --type=LoadBalancer --port=80 --target-port=80
```

### Retrieve ip
```
kubectl get services
```
Note: External ip takes a bit of time to create (1-2 mins).

### Update container in kubernetes cluster
```
kubectl set image deployment/atsite atsite=atsite:latest

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
